{-# LANGUAGE OverloadedStrings, OverloadedLists, LambdaCase, GADTSyntax, RankNTypes #-}
{-# LANGUAGE DeriveGeneric, TupleSections, BangPatterns, MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf, TypeFamilies, TypeOperators, FunctionalDependencies, MonadComprehensions #-}
{-# LANGUAGE BinaryLiterals, ParallelListComp, PartialTypeSignatures, RecordWildCards, PatternSynonyms #-}
{-# LANGUAGE EmptyCase, InstanceSigs, KindSignatures, DeriveFunctor, DeriveFoldable, DeriveTraversable #-}
{-# LANGUAGE ConstraintKinds, ExplicitNamespaces #-}

-- The list of language extensions is used by default for all modules. Not all extensions are actually used.

{-# OPTIONS_HADDOCK show-extensions #-}
{-|
Module      : Mango.Parser.Lexer
Description : Turn character-stream into token-stream
Copyright   : (c) Martin Zeller, 2018
License     : BSD2
Maintainer  : Martin Zeller <mz.bremerhaven@gmail.com>
Stability   : experimental
Portability : POSIX
-}

module Mango.Parser.Lexer ( lex
                          ) where

import Prelude hiding (lex)

import           Control.Monad (void)
import qualified Data.Char as Char
import           Data.Semigroup
import           Data.Text (Text(..))
import qualified Data.Text as Text
import           Text.Parsec
import           Text.Parsec.Char
import           Text.Parsec.Token

data Lexeme = Keyword Text
            | Ident Text
            | Number (Either Integer Double)
            | Operator Text
            | LParen
            | RParen
            | LBrace
            | RBrace
            | LSquare
            | RSquare
            | LArrow
            | RArrow
            | Equals
            | NEQ
            | Colon
            | DColon
            | Ampersand
            | Comma
            | Period
            | Semicolon
            | Exclamation
            deriving (Show, Eq, Ord)

type Lexer a = forall u. Parsec Text u a

lex :: String -> Text -> Either ParseError [Lexeme]
lex = parse (many (skipWhitespace >> parseToken <* skipWhitespace) <* eof)

keywords :: [String]
keywords = [ "fn"
           , "if"
           , "while"
           , "mod"
           , "return"
           , "pub"
           ]

parseToken :: Lexer Lexeme
parseToken = skipWhitespace >> choice
    [ try (string "->") *> pure RArrow
    , try (string "<-") *> pure LArrow
    , try (string "::") *> pure DColon
    , char '(' *> pure LParen
    , char ')' *> pure RParen
    , char '{' *> pure LBrace
    , char '}' *> pure RBrace
    , char '=' *> pure Equals
    , char ':' *> pure Colon
    , char ';' *> pure Semicolon
    , Operator . Text.pack <$> many1 parseSymbol
    , parseKeyword <?> "keyword"
    , parseIdentifier <?> "identifier"
    ]

skipWhitespace :: Lexer ()
skipWhitespace = skipMany (void space <|> void parseComment)

parseComment :: Lexer String
parseComment =  try (string "//") >> manyTill anyChar (void newline <|> eof)

parseKeyword :: Lexer Lexeme
parseKeyword = Keyword . Text.pack <$> foldr1 (<|>) (try . string <$> keywords)

parseSymbol :: Lexer Char
parseSymbol = oneOf "-" <|> satisfy Char.isSymbol

parseIdentifier :: Lexer Lexeme
parseIdentifier = Ident . Text.pack <$> (underscoreIdent <|> normalIdent)
  where identChar :: Lexer Char
        identChar = char '_' <|> satisfy Char.isAlphaNum

        underscoreIdent :: Lexer String
        underscoreIdent = try (string "_" <> many1 identChar)

        normalIdent :: Lexer String
        normalIdent = try $ many1 identChar
