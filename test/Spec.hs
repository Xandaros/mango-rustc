import Prelude hiding (lex)

import Control.Monad (sequence, forM_, filterM)
import Control.Monad.IO.Class (liftIO)
import Data.Either (isRight)
import qualified Data.Text as Text
import System.Directory (getDirectoryContents, doesFileExist)
import Test.Hspec

import Mango.Parser.Lexer

lexerTestDir :: FilePath
lexerTestDir = "test/lexer/"

main :: IO ()
main = do
    lexerFiles <- (fmap (lexerTestDir ++) <$> getDirectoryContents lexerTestDir) >>= filterM doesFileExist
    lexerContents <- sequence $ fmap Text.pack . readFile <$> lexerFiles
    let lexerTests = zip lexerFiles lexerContents
    hspec $
        describe "Lexer" $
            forM_ lexerTests $ \(test, testContent) ->
                it test $ lex test testContent `shouldSatisfy` isRight
