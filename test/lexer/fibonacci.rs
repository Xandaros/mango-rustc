fn main()
{
    fibonacci::main();
}

mod fibonacci
{
    pub fn main()
    {
        recursive(23);
    }

    fn recursive(n: i32) -> i32
    {
        if n > 2
        {
            return recursive(n-2) + recursive(n-1);
        }
        return n;
    }
}
